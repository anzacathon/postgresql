
Install the packages
--------------------

```
sudo apt update
sudo apt install fuse postgresql pgloader
```

Setup mount points for the IPFS client
--------------------------------------

```
sudo mkdir -p /ipfs /ipns
sudo chown $USER /ipfs /ipns
```

Install and run the IPFS client
-------------------------------

For any other version of the IPFS binary, see the links at the bottom.

```
wget https://dist.ipfs.io/go-ipfs/v0.4.23/go-ipfs_v0.4.23_linux-amd64.tar.gz
tar xzf go-ipfs_v0.4.23_linux-amd64.tar.gz
./go-ipfs/ipfs daemon --mount
```

Setup the PostgreSQL database
-----------------------------

Replace daniel with your own UNIX username.

```
sudo su - postgres
createdb anzacathon
createuser daniel
psql -d anzacathon -c "CREATE EXTENSION postgis;"
exit

mkdir -p ~/ws
cd ~/ws
git clone https://gitlab.com/anzacathon/postgresql anzacathon-postgresql
cd anzacathon-postgresql
pgloader anzacathon.pgload

psql -d anzacathon -f schema.sql

psql anzacathon

anzacathon=> \dt
                        List of relations
 Schema |                 Name                  | Type  | Owner  
--------+---------------------------------------+-------+--------
 public | cwgc_casualty                         | table | daniel
 public | iwmcemeteries                         | table | daniel
 public | naa_documents                         | table | daniel
 public | naa_documents_attrs                   | table | daniel
 public | naa_documents_notes_phrases_scores    | table | daniel
 public | people                                | table | daniel
 public | people_attr                           | table | daniel
 public | roll_of_honour                        | table | daniel
 public | roll_of_honour_attr                   | table | daniel
 public | tracesofwar_sites                     | table | daniel
 public | tracesofwar_sites_desc_phrases_scores | table | daniel
```



Links / references
------------------

Main Anzacathon web site:
  https://anzacathon.com

Accessing IPFS resources with FUSE:
  https://github.com/ipfs/go-ipfs/blob/master/docs/fuse.md

Latest IPFS client downloads for various architectures:
  https://dist.ipfs.io/

