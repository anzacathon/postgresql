
-- CWGC: iwmcemeteries

ALTER TABLE iwmcemeteries
ADD location_gis geography(POINT, 4326);

-- fix a broken row in iwmcemeteries
UPDATE iwmcemeteries
SET latitude = '51.70433', longitude = '5.30499'
WHERE cemetery = '54238';

UPDATE iwmcemeteries
SET location_gis = ('SRID=4326;POINT('||latitude||' '||longitude||')')::geography
WHERE latitude != 'NULL' AND longitude != 'NULL' AND latitude != '' AND longitude != '';

-- TracesOfWar: tracesofwar_sites

ALTER TABLE tracesofwar_sites
ADD location_gis geography(POINT, 4326);

UPDATE tracesofwar_sites
SET location_gis = ('SRID=4326;POINT('||latitude||' '||longitude||')')::geography;

-- Views

CREATE VIEW anzac_sites AS
SELECT 'CWGC' AS source, cemetery AS id, cemetery_desc AS description, location_gis FROM iwmcemeteries
UNION
SELECT 'TracesOfWar' AS source, id::text, name, location_gis FROM tracesofwar_sites;

